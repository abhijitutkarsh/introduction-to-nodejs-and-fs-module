var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.stat(PathToFile,(err,data)=>
        {
            if(err)
            {
                reject("Error occured while calculating stats");
            }
            else{
                var obj={
                    birthTime:data.birthtimeMs,
                    modifiedTime:data.mtimeMs,
                    size:data.size,
                    
                }
                if(data.isFile())
                obj.type="File";
                else
                obj.type="Directory";
                resolve(obj);
                
                }
        });
    });

}
module.exports=func;
