var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.mkdir(PathToFile,err=>
        {
            if(!err)
            resolve("New directory created");
            else
            reject("Error occured while making directory");
        });
    });
}
module.exports=func;
