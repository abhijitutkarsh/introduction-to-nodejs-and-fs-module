var fs=require("fs");
var path=require("path")
function func(PathToFile)
{
    var files=getfiles(PathToFile);
    for(var i=0;i<files.length;i++)
    {
        var finalPath=path.join(PathToFile,files[i]);
        var isfile=isFile(finalPath);
        if(isfile){}
        else
        {
            var files2=getfiles(finalPath);
            files=[...files.slice(0,i),files2,...files.slice(i+1,)];
        }
    }
    return(files);

}
function getfiles(path1)
{
    return new Promise((resolve,reject)=>
    {
        fs.readdir(path1,(err,data)=>
        {
            if(err)
            reject("Error occured while reading directory");
            else
            resolve(data);
        });
    });
}
function isFile(path1)
{
    return new Promise((resolve,reject)=>
    {
        fs.stat(path1,(err,stats)=>
        {
            if(err)
            reject("err");
            else
            resolve(stats.isFile());
        });
    });
}
module.exports=func;
