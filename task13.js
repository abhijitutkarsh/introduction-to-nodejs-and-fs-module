var fs=require("fs");
var path=require("path");
function func(PathToFile,index)
{
    return new Promise((resolve,reject)=>
    {
        fs.readdir(PathToFile,(err,data)=>
        {
            if(err)
            reject("Error occured while fetching files from Directory");
            else
            {
                var finalPath=path.join(PathToFile,data[index]);
                fs.readFile(finalPath,"utf8",(err,stats)=>
                {
                    if(!err)
                    {
                        fs.stat(finalPath,(err,stats1)=>{
                            if(!err)
                            {
                        var obj={
                            data:stats,
                            filename:data[index],
                            size:stats1.size,
                            birthTime:stats1.birthtimeMs

                        }
                        resolve(obj);
                            }
                        });
                    }
                });
            }
        });
    });
}
module.exports=func;

