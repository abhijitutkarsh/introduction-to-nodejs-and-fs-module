var fs=require("fs");
var path=require("path");
function func(PathSource,PathDest)
{
    return new Promise((resolve,reject)=>
    {
        fs.access(PathSource,err=>
        {
            if(!err)
            {
                fs.readFile(PathSource,"utf8",(err,data)=>
                {
                    if(!err)
                    {
                        fs.access(PathDest,err=>{
                            if(!err)
                            {
                                fs.appendFile(PathDest,data,err=>
                                {
                                    if(!err)
                                    resolve("Content written to file successfully");
                                    else
                                    reject("Source File does not exists");
                                });

                            }
                            else
                            {
                                fs.writeFile(PathDest,data,err=>
                                {
                                    if(!err)
                                    resolve("Content written to file successfully");
                                    else
                                    reject("Source File does not exists");
                                })
                            }
                        });
                    }
                });
            }
            else
            reject("Source File does not exists");
        });

    });
}
module.exports=func;

