var path=require("path");
function func(PathToFile,pos,str)
{
    let baseName=path.basename(PathToFile);
    let dirName=path.dirname(PathToFile);
    let finalPath;
    if(pos==="start")
    {
        finalPath=path.join(str,PathToFile);
    }
    else{
        finalPath=path.join(PathToFile,str);
    }
    var obj=
    {
        baseName:baseName,
        dirName:dirName,
        finalPath:finalPath
    }
    return(obj);
}
module.exports=func;
