var fs=require("fs");
var task04=require("./task04");
var task21=require("./task21");
var task22=require("./task22");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        task04(PathToFile).then(res=>
        {
         task22(PathToFile).then(res=>
         {
             resolve("File exists : Directory removed");
         });   
        }).catch(res=>
        {
            task21(PathToFile).then(res=>
            {
                task22(PathToFile).then(res=>
                {
                    resolve("New directory created : Directory removed");
                });
            });
        });
    });
}
module.exports=func;
