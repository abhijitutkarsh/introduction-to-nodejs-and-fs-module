var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {   
        fs.access(PathToFile,err=>
        {
            if(!err)
            resolve("File exists");
            else
            reject("File does not exists");
        });
    });
}
module.exports=func;
