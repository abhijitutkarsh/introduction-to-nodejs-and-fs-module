var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.stat(PathToFile,(err,data)=>
        {
            if(err)
            {
                reject("Error occured while calculating stats");
            }
            else
            {
                resolve(data.size);
            }
        });
    });
    
}
module.exports=func;
