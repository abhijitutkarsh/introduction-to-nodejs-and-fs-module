var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.unlink(PathToFile,err=>
        {
            if(!err)
            resolve("File deleted successfully");
            else
            reject("error deleting file");
        });
    });
}
module.exports=func;
