var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.readFile(PathToFile,"utf8",(err,data)=>
        {
            if(err)
            reject("File does not exists");
            else
            resolve(data);
        })

    });
}
module.exports=func;

