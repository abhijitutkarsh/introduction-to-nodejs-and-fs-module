var fs=require("fs");
var path=require("path");
function func(PathToFile,data)
{
    return new Promise((resolve,reject)=>
    {
        fs.access(PathToFile,err=>
        {
            if(err)
            {
                fs.writeFile(PathToFile,data,err=>
                {
                    if(!err)
                    resolve("Content written to file successfully");
                });
            }
            else
            reject("File already exists provide another name");
        });
    });
}
module.exports=func;
