var fs=require("fs");
var path=require("path");
function func(PathToFile,data)
{
    return new Promise((resolve,reject)=>
    {
        fs.access(PathToFile,err=>
        {
            if(!err)
            {
                fs.appendFile(PathToFile,data,err=>
                {
                    if(!err)
                    resolve("Content appended to file successfully");
                });
            }
            else
            reject("File does not exists provide another name");
        });
    });
}
module.exports=func;

