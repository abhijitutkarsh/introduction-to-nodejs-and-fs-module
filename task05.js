var fs=require("fs");
var path=require("path");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    
    {
        fs.readdir(PathToFile,(err,files)=>
        {
            if(err)
            reject("Error occured while reading directory");
            else
            resolve(files);
        });

    });
}
module.exports=func;
