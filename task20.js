var fs=require("fs");
var path=require("path");
var PathToFile=require("./task04");
function func(PathToFile,content)
{
    return new Promise((resolve,reject)=>
    {
        fs.access(PathToFile,err=>
        {
            if(!err)
            {
                fs.appendFile(PathToFile,content,err=>
                {
                    if(!err)
                    resolve("Data appended successfully");
                });
            }
            else
            reject("Cannot append data because : File does not exists");
        });
    });
}
module.exports=func;
