const fs = require('fs');
const path = require('path');

const main = (pathToDir) => {
  return new Promise((resolve, reject) => {
    fs.readdir(pathToDir, async (error, files) => {
      if (error) {
        reject('Error occured while traversing directory');
      } else {
        let obj = { countDir: 0, countFile: 0 };
        for (let i = 0; i < files.length; i++) {
          if (files[i].includes('.')) obj.countFile++;
          else {
            obj.countDir++;
            let newPath = path.join(pathToDir, files[i]);

            let retObj = await main(newPath);

            obj.countFile += retObj.countFile;
            obj.countDir += retObj.countDir;
          }
        }

        resolve(obj);
      }
    });
  });
};

module.exports = main;


