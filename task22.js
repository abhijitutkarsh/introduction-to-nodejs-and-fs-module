var fs=require("fs");
var path=require("path");
var PathToFile=require("./task04");
function func(PathToFile)
{
    return new Promise((resolve,reject)=>
    {
        fs.access(PathToFile,err=>
        {
            if(!err)
            {
                fs.rmdir(PathToFile,err=>
                {
                    if(!err)
                    resolve("File exists : Directory removed");
                });
            }
            else
            reject("cannot remove directory because : File does not exists");
        });
    });
}
module.exports=func;
